interface Background {
  url: string;
  name: string;
}

const BACKGROUND_LIST: Background[] = [
  {
    url: '/assets/1.jpg',
    name: '大盘/上证/999999'
  },
  {
    url: '/assets/2.jpg',
    name: '大盘/上证总表'
  },
  {
    url: '/assets/3.jpg',
    name: '个股/个股交易策略'
  },
  {
    url: '/assets/4.jpg',
    name: '个股/早盘个股操盘监控'
  }
];

export default BACKGROUND_LIST;
