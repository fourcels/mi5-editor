import { Component, OnInit } from '@angular/core';
import uniqueId from 'lodash/uniqueId';
import BACKGROUND_LIST from './background-list';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'app';
  backgroundList  = BACKGROUND_LIST;
  background = this.backgroundList[0];
  width = 100;
  left = 100;
  top = 100;
  fontSize = 20;
  _canvas;
  output = '';
  get textList () {
    return this._canvas._objects;
  }
  ngOnInit(): void {
    this._canvas = new fabric.Canvas('canvas', {
      backgroundColor: '#000',
      selection: false
    });
    this.updateBackground(this.background);
  }
  onExport () {
    this.output = this.generateOutput();
  }
  onImport () {
    const canvas = this._canvas;
    canvas.clear();
    canvas.loadFromJSON(this.output, canvas.renderAll.bind(canvas), function (_, object) {
      if (object.type === 'textbox') {
        object.backgroundColor = '#fff';
      }
    });
  }
  onPreview () {
    const output = this.generateOutput();
    window.open(`/preview.html?json=${btoa(encodeURIComponent(output))}`);
  }
  generateOutput() {
    const canvas = this._canvas;
    const {backgroundImage, objects} = canvas.toJSON(['action']);
    const scale = canvas.width / backgroundImage.width;
    const backgroundImage2 = {
      type: backgroundImage.type,
      src: backgroundImage.src,
      height: backgroundImage.height,
      width: backgroundImage.width,
      scaleX: scale,
      scaleY: scale,
      fill: backgroundImage.fill
    };
    const objects2 = objects.map(item => {
      return {
        type: item.type,
        width: item.width,
        left: item.left,
        top: item.top,
        textAlign: item.textAlign,
        fontSize: item.fontSize,
        fill: item.fill,
        text: item.text,
        hasControls: false,
        action: item.action,
        underline: item.underline,
      };
    });
    return JSON.stringify({
      backgroundImage: backgroundImage2,
      objects: objects2
    });
  }
  updateBackground(background) {
    const canvas = this._canvas;
    canvas.clear();
    fabric.Image.fromURL(background.url, function(img) {
      // add background image
      const scale = canvas.width / img.width;
      canvas.setHeight(Math.floor(img.height * scale));
      canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas), {
         scaleX: scale,
         scaleY: scale
      });
   });
  }
  onAddItem() {
    const {fontSize, top, left, width} = this;
    const text = new fabric.Textbox('text' + uniqueId(), {
      hasControls: false,
      fontSize,
      top,
      left,
      width,
      breakWords: true,
      textAlign: 'center',
      backgroundColor: '#fff',
    });
    this._canvas.add(text);
  }
  onRemoveItem (item) {
    if (confirm(`确定删除 '${item.text}' ?`)) {
      this._canvas.remove(item);
    }
  }
  onInput (value, prop, item) {
    item.set(prop, value).setCoords();
    this._canvas.renderAll();
  }
  onSet (item){
    item.set("underline",true).set("fill","#0b42d2").setCoords();
    this._canvas.renderAll();
  }
}
